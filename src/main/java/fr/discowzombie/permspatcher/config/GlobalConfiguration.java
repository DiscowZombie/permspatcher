/*
 * MIT License
 *
 * Copyright (c) 2023 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package fr.discowzombie.permspatcher.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

public class GlobalConfiguration {

    @NotNull
    public Permissions permissions = new Permissions();

    @NotNull
    public Commands commands = new Commands();

    public static class Permissions {

        @NotNull
        @JsonProperty("set_default")
        public SetDefault setDefault = new SetDefault();

        @NotNull
        public Register register = new Register();

        public static class SetDefault {

            @NotNull
            @JsonProperty("start_with")
            public StartWith startWith = new StartWith();

            @NotNull
            public Exact exact = new Exact();

            public static class StartWith {

                @NotNull
                @JsonProperty(value = "true")
                public List<String> t = List.of();

                @NotNull
                @JsonProperty(value = "false")
                public List<String> f = List.of();

                @NotNull
                @JsonProperty(value = "op")
                public List<String> op = List.of();

                @NotNull
                @JsonProperty(value = "non_op")
                public List<String> nonOp = List.of();
            }

            public static class Exact {

                @NotNull
                @JsonProperty(value = "true")
                public List<String> t = List.of();

                @NotNull
                @JsonProperty(value = "false")
                public List<String> f = List.of();

                @NotNull
                @JsonProperty(value = "op")
                public List<String> op = List.of();

                @NotNull
                @JsonProperty(value = "non_op")
                public List<String> nonOp = List.of();
            }
        }

        public static class Register {

            @NotNull
            @JsonProperty(value = "true")
            public List<String> t = List.of();

            @NotNull
            @JsonProperty(value = "false")
            public List<String> f = List.of();

            @NotNull
            @JsonProperty(value = "op")
            public List<String> op = List.of();

            @NotNull
            @JsonProperty(value = "non_op")
            public List<String> nonOp = List.of();
        }
    }

    public static class Commands {

        @NotNull
        @JsonProperty("set_permission")
        public Map<String, List<String>> setPermission = Map.of();

        @NotNull
        @JsonProperty("hide_and_disable")
        public List<String> hideAndDisable = List.of();
    }

}
