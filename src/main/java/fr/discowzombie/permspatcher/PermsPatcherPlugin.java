/*
 * MIT License
 *
 * Copyright (c) 2023 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package fr.discowzombie.permspatcher;

import fr.discowzombie.permspatcher.config.GlobalConfiguration;
import fr.discowzombie.permspatcher.configuration.JsonConfiguration;
import fr.discowzombie.permspatcher.listeners.DisableHideListener;
import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;

public final class PermsPatcherPlugin extends JavaPlugin {

    private static final String CONFIG_FILE_NAME = "config.json";

    @Override
    public void onEnable() {
        @NotNull GlobalConfiguration configuration;

        try {
            // Create plugin data folder if not exist
            if (!this.getDataFolder().exists()) {
                if (!this.getDataFolder().mkdirs()) {
                    this.getLogger().log(Level.WARNING, "Unable to create plugin data-folder, read-only file system?");
                }
            }

            configuration = JsonConfiguration.copyAndLoadJson(
                    Objects.requireNonNull(getResource(CONFIG_FILE_NAME)),
                    new File(this.getDataFolder() + File.separator + CONFIG_FILE_NAME),
                    GlobalConfiguration.class);
        } catch (IOException e) {
            this.getLogger().log(Level.SEVERE, "Unable to load configuration file", e);
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        // Some plugins bypass the Bukkit Command class
        Bukkit.getPluginManager().registerEvents(new DisableHideListener(configuration), this);

        // Update effectives permissions
        Bukkit.getScheduler().runTask(this, () -> {
            final var pluginManager = Bukkit.getPluginManager();

            // Register requested permissions
            {
                configuration.permissions.register.t.forEach(p -> registerNewPermission(p, PermissionDefault.TRUE));
                configuration.permissions.register.f.forEach(p -> registerNewPermission(p, PermissionDefault.FALSE));
                configuration.permissions.register.nonOp.forEach(p -> registerNewPermission(p, PermissionDefault.NOT_OP));
                configuration.permissions.register.op.forEach(p -> registerNewPermission(p, PermissionDefault.OP));
            }

            // Patch permissions (startWith)
            {
                for (final var sw : configuration.permissions.setDefault.startWith.t) {
                    for (final var perm : pluginManager.getPermissions())
                        if (perm.getName().startsWith(sw))
                            perm.setDefault(PermissionDefault.TRUE);
                }
                for (final var sw : configuration.permissions.setDefault.startWith.f) {
                    for (final var perm : pluginManager.getPermissions())
                        if (perm.getName().startsWith(sw))
                            perm.setDefault(PermissionDefault.FALSE);
                }
                for (final var sw : configuration.permissions.setDefault.startWith.nonOp) {
                    for (final var perm : pluginManager.getPermissions())
                        if (perm.getName().startsWith(sw))
                            perm.setDefault(PermissionDefault.NOT_OP);
                }
                for (final var sw : configuration.permissions.setDefault.startWith.op) {
                    for (final var perm : pluginManager.getPermissions())
                        if (perm.getName().startsWith(sw))
                            perm.setDefault(PermissionDefault.OP);
                }
            }

            // Re-patch permissions (exact)
            configuration.permissions.setDefault.exact.t.forEach(s -> setPermissionDefault(s, PermissionDefault.TRUE));
            configuration.permissions.setDefault.exact.f.forEach(s -> setPermissionDefault(s, PermissionDefault.FALSE));
            configuration.permissions.setDefault.exact.nonOp.forEach(s -> setPermissionDefault(s, PermissionDefault.NOT_OP));
            configuration.permissions.setDefault.exact.op.forEach(s -> setPermissionDefault(s, PermissionDefault.OP));

            // Patch plugins commands
            for (final var entry : configuration.commands.setPermission.entrySet()) {
                final var permStr = entry.getKey();
                for (final var cmd : entry.getValue()) {
                    setPluginCommandPermission(cmd, permStr);
                }
            }
        });
    }

    private void registerNewPermission(final @NotNull String str, final @NotNull PermissionDefault permissionDefault) {
        final var pluginManager = Bukkit.getPluginManager();
        final var perm = new Permission(str, permissionDefault);
        if (pluginManager.getPermission(perm.getName()) == null)
            pluginManager.addPermission(perm);
    }

    private void setPermissionDefault(final @NotNull String str, final @NotNull PermissionDefault permissionDefault) {
        final var perm = Bukkit.getPluginManager().getPermission(str);
        if (perm != null)
            perm.setDefault(permissionDefault);
    }

    private void setPluginCommandPermission(final @NotNull String command, final @NotNull String permStr) {
        final var cmd = Bukkit.getCommandMap().getCommand(command);
        if (cmd != null)
            cmd.setPermission(permStr);
    }

}
