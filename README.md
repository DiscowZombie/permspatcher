# PermsPatcher

A lot of plugins bypass the Bukkit API to register commands and permissions. This lead to *commands leaks*, i.e. sending
commands to users, even if they aren't access to them.

The goal of this project is to fix those leaks by re-parenting commands with appropriates permissions.

## Configuration

The default (commented) configuration is shown below. This configuration provide a quick started for server wanting to
lock Minecraft commands (e.g. `/me`) and also include support for common plugins (FAWE, WorldGuard, LuckPerms,
HolographicDisplay and Spark). Added support for other plugins is straightforward.

```json
{
  // The file is decouped in two sections ; permissions and commands.
  // Permission relative
  "permissions": {
    // Set default permission access for permissions
    "set_default": {
      "start_with": {
        // All permissions that start with 'minecraft.' or 'bukkit.' will be set to PermissionDefault.OP.
        // This is enough to prevent accessing of commands such as /me, /tell, etc.
        "op": [
          "minecraft.",
          "bukkit."
        ]
      },
      "exact": {
        // Even if the permissions are included above, set their access to PermissionDefault.TRUE.
        // If you remove the two permissions below, Bukkit#broadcastMessage(String) will not be received until the user is OP
        // (since all permissions starting with 'bukkit.' is set to PermissionDefault.OP [see above])
        "true": [
          "bukkit.broadcast",
          "bukkit.broadcast.user"
        ]
      }
    },
    // Register new permissions
    "register": {
      // To be used below, the permission 'pp.disabled_commands' will be an alias of PermissionDefault.FALSE
      // Please keep in mind that granting this permissiopn thrgough a permission plugin (e.g. LuckPerms) will still work
      // and allow users to use commands behind this permission.
      "false": [
        "pp.disabled_commands"
      ],
      // To be used below, the permission 'pp.operators_commands' will be an alias of PermissionDefault.OP
      "op": [
        "pp.operators_commands"
      ]
    }
  },
  // Command relative
  "commands": {
    "set_permission": {
      "pp.disabled_commands": [
        // All commands listed below will get their permission set to 'pp.disabled_commands'
        // (i.e. nobody can use them unless some groups have the permission 'pp.disabled_commands' set to 'true' inside your permission plugin)
        "icanhasbukkit"
      ],
      "pp.operators_commands": [
        // All commands listed below will get their permission set to 'pp.operators_commands' 
        // (i.e. limit their access to OP's with the current setup)
        "hologram",
        "lp",
        "luckperms",
        "lpb",
        "luckpermsbungee",
        "spark",
        "tps",
        "co"
      ]
    },
    // Some (bad) plugins completely bypass the Bukkit API while registering their commands. As a result, trying to
    // set permissions for commands will not work since commands are NOT properly registered inside Bukkit.
    // The list below contains commands that will not be sent to players while requesting available commands. Furthermore,
    // inputting one of those word prefixed by '/' will produce an event cancellation, to be sure that commands never
    // reach the backend plugin.
    // If one plugin register the same command with multiple aliases (e.g. '/fastasyncworldedit' and '/fawe') all variants
    // should be included.
    // Lastly, players with the permission 'pp.show_hide_and_disable' will be able to receive and send the commands below.
    // By default, this permission is granted to nobody.
    "hide_and_disable": [
      "fastasyncworldedit",
      "fawe",
      "region",
      "regions",
      "rg",
      "we",
      "worldedit",
      "bungee",
      "god",
      "heal",
      "lpb",
      "luckpermsbungee",
      "slay",
      "ungod"
    ]
  }
}
```

The plugin only register one permission for himself. This permission is `pp.show_hide_and_disable` (granted to nobody by
default). As the name suggests, players with this permission can receive commands listed in `commands.hide_and_disable`
inside the configuration. You can grant this permission through a permission plugin if you want (e.g. LuckPerms).
